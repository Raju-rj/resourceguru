<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $vehicles=Vehicle::all();
        return view('front-end/vehicle/index',compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front-end/vehicle/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->only('name','bookable','picture','registration_number','notes');

        $picturename=$request->name.'-'.$request->picture->getClientOriginalName();
        $request->picture->move(public_path('upload/pictures'),$picturename);
        $data['picture']=$picturename;

        $picture=Vehicle::create($data);
        return redirect('/vehicle');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle=Vehicle::find($id);
        return view('front-end/vehicle/show',compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Vehicle::find($id);

        return view('front-end/vehicle/edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exit=Vehicle::find($id);
        $data=$request->all();
        if($request->picture)
        {
            $picturename=$request->name.'-'.$request->picture->getClientOriginalName();
            $request->picture->move(public_path('upload/pictures'),$picturename);
            $data['picture']=$picturename;
        }
        else
        {
            $data['picture']=$exit->picture;
        }

        $exit->update($data);

        return redirect('/vehicle');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $delete=Vehicle::find($id);
       $delete->delete();
       return redirect('vehicle');
    }
}
