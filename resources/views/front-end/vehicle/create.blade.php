@extends('master_layout.layout')
@section('content')
    <div class="container"><br>

        <div class="col-md-6 offset-md-3">

            <div class="card">

                <h5 class="text-center card-header">Vehicle Create</h5>

                <div class="card-body">

                    <div class="col-sm-12">
                        <form action="/vehicle" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="name">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" name="bookable" type="checkbox" value="1" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Bookable
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">photo</label>
                                <input type="file" name="picture" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Registation Number</label>
                                <input type="text" name="registration_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="registration number">
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea cols="5" rows="5" name="notes" class="form-control" id="notes" aria-describedby="emailHelp" placeholder="notes"></textarea>

                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>


        </div>

@endsection