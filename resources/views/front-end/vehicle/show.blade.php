@extends('master_layout.layout')
@section('content')

    <div class="container"><br>

        <div class="col-md-8 offset-md-2">

            <div class="card">
                <div class="card-header">

                       <a href="{{url('vehicle/')}}" class="btn btn-dark"><i class="fa fa-arrow-circle-left"></i> Back</a>
                </div>

                <div class="card-body">

                    <div class="col-sm-12">
                        <table class="table tablr-border">

                            <tr><th>Name           :</th><td>{{$vehicle->name}}</td></tr>
                            <tr><th>Bookable       :</th><td>{{$vehicle->bookable}}</td></tr>
                            <tr><th>Picture        :</th> <td><img src="{{asset('upload/pictures/'.$vehicle->picture)}}" width="80px" height="50px"></td></tr>
                            <tr><th>Registration No:</th><td>{{$vehicle->registration_number}}</td></tr>
                            <tr><th>Note           :</th><td>{{$vehicle->notes}}</td></tr>


                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection